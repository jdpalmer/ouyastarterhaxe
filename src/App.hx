import flash.ui.GameInputDevice;
import flash.ui.GameInput;
import flash.ui.GameInputControl;
import flash.events.GameInputEvent;
import flash.geom.Rectangle;
//import flash.display.Sprite;
import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.errors.Error;
//import flash.ui.GameInputControlName;
import flash.Lib;
import flash.Vector;
import flash.utils.ByteArray;
import net.hires.debug.Stats;
import starling.core.Starling;
import starling.display.Sprite;
//import starling.events.Event;

// import openfl.events.JoystickEvent;

//#if android
//import openfl.utils.JNI;
//import tv.ouya.console.api.OuyaController;
//#end

/**
 * ...
 * @author Mike McMullin
 */

class App extends Sprite 
{
  var controllers:Array<ControllerSprite>;
  private static var gameInput:GameInput;

	
	public function new ()
	{
		super();
		
		trace("== OUYA CONTROLLER TEST (" + Date.now() + ") ==");
		
                //gameInput = new GameInput();
                gameInput.addEventListener(GameInputEvent.DEVICE_ADDED, handleDeviceAttached);
                gameInput.addEventListener(GameInputEvent.DEVICE_REMOVED, handleDeviceRemoved);

trace("one");
                handleDeviceAttached(null);
          

/*
		// init controllers
		#if android
		var getContext = JNI.createStaticMethod ("org.haxe.nme.GameActivity", "getContext", "()Landroid/content/Context;", true);
		OuyaController.init (getContext());
		#end
*/
		
		// init controller graphics
		controllers = [];
		for (i in 0...4)
		{
			controllers.push(createController(i));
			addChild(controllers[i]);
		}

		// add listeners
//		stage.addEventListener(JoystickEvent.AXIS_MOVE, onJoystickAxisMove);
//		stage.addEventListener(JoystickEvent.BUTTON_DOWN, onJoystickButtonDown);
//		stage.addEventListener(JoystickEvent.BUTTON_UP, onJoystickButtonUp);
//		stage.addEventListener(JoystickEvent.HAT_MOVE, onJoystickHatMove);
		
	}
	
	private function createController(id:Int):ControllerSprite
	{
		var controller:ControllerSprite = new ControllerSprite();
		controller.y = 300;
		controller.x = id  * 400 + 160;
		return controller;
	}
	
	
	// EVENT HANDLERS

private static function stage_onResize (e):Void {
var stage = flash.Lib.current.stage;
var mStarling = Starling.current;
// http://forum.starling-framework.org/topic/starling-stage-resizing
        trace("RESIZE");
    trace("flash.Lib.current.stage.stageWidth: " + flash.Lib.current.stage.stageWidth);
    trace("flash.Lib.current.stage.stageHeight: " + flash.Lib.current.stage.stageHeight);

var viewPortRectangle:Rectangle = new Rectangle();
  viewPortRectangle.width = stage.stageWidth;
  viewPortRectangle.height = stage.stageHeight;
  Starling.current.viewPort = viewPortRectangle;
 
  mStarling.stage.stageWidth = stage.stageWidth;
  mStarling.stage.stageHeight = stage.stageHeight;

    }
	

//  protected 
function handleDeviceAttached(e:GameInputEvent)
                {
                        trace("Device is added\n");
//                        GameInputControlName.initialize(e.device);
                        
                        var _controls:Vector<String>;
                        for (k in 0... GameInput.numDevices) {
                          trace("for: " + k);
                                var _device = GameInput.getDeviceAt(k);
                                _controls = new Vector<String>();
                                _device.enabled = true;
                                
                                for (i in 0..._device.numControls) {
                                        var control:GameInputControl = _device.getControlAt(i);
                                        control.addEventListener(Event.CHANGE,onChange);
                                        _controls[i] = control.id;
                                }
                                
                                _device.startCachingSamples(30, _controls);
                                
                                
                        }
trace("nfor");
                        
//                        for(j in 0..._controls.length)
  //                      {
    //                            trace(_controls[j]);
      //                  }
                        
//                        flash.Lib.current.stage.addEventListener(Event.ENTER_FRAME, getCachedSamples);
                      trace("out");  
                }


public function handleDeviceRemoved(event:GameInputEvent)
		{
			trace("Device is removed\n");
		}


//stage.addEventListener(Event.ENTER_FRAME, getCachedSamples);
                        
        //        }       
                
//                protected 
private function onChange(event:Event)
                {
                        var control:GameInputControl = cast(event.target, GameInputControl);

                  //var _device = null;
                  var deviceIndex = -1;
                  for (k in 0... GameInput.numDevices) {
                    if (control.device == GameInput.getDeviceAt(k)) {
                      deviceIndex = k;
                      break;
                    }
                  }
                  

                        //tf.scrollV++;
                        trace("device=" + deviceIndex + " control=" + control.id +" value="+control.value+" \n");
                }
                
//                protected 
private function getCachedSamples(event:Event)
                {
trace("getcs");
                        var data:ByteArray = new ByteArray();
                        var _device:GameInputDevice;
                        _device = GameInput.getDeviceAt(0);
                  var completed;

                        try
                        {
                                completed = _device.getCachedSamples(data, true);
                          if(completed > 0 && data.length > 0)
                          {
                            //tf.scrollV++;
                            trace("Number of samples are "+completed+" and byte length is "+data.length+" \n");
                          }

                        } 
                        catch(e:Error)
                        {
                                trace("FAIL \n");
                        }
                        
                }
/*
	private function onJoystickAxisMove(e:JoystickEvent):Void 
	{
		var player = OuyaController.getPlayerNumByDeviceId(e.device);
		var controller = OuyaController.getControllerByDeviceId(e.device);
		
		var leftX:Float = e.axis[OuyaController.AXIS_LS_X];
		var leftY:Float = e.axis[OuyaController.AXIS_LS_Y];
		var rightX:Float = e.axis[11]; //update with OuyaController.AXIS_RS_X
		var rightY:Float = e.axis[14]; //update with OuyaController.AXIS_RS_Y
		
		controllers[player].updateJoysticks(leftX, leftY, rightX, rightY);
	}
	
	private function onJoystickHatMove(e:JoystickEvent):Void 
	{
		var player = OuyaController.getPlayerNumByDeviceId(e.device);
		var controller = OuyaController.getControllerByDeviceId(e.device);
		controllers[player].updateDpad(e.x, e.y);
	}
	
	private function onJoystickButtonUp(e:JoystickEvent):Void 
	{
		var player = OuyaController.getPlayerNumByDeviceId(e.device);
		var controller = OuyaController.getControllerByDeviceId(e.device);
		controllers[player].updateButton(e.id, false);
	}
	
	private function onJoystickButtonDown(e:JoystickEvent):Void 
	{
		var player = OuyaController.getPlayerNumByDeviceId(e.device);
		var controller = OuyaController.getControllerByDeviceId(e.device);
		controllers[player].updateButton(e.id, true);
	}
*/


  static function main() {

    // Setup stage
    var stage = flash.Lib.current.stage;
//    stage.align = StageAlign.TOP_LEFT;
//    stage.scaleMode = StageScaleMode.NO_SCALE;
    stage.addEventListener(Event.RESIZE, stage_onResize);

    // Setup Starling
    var starling = new Starling(App, stage);
    starling.antiAliasing = 1;
    starling.start();

    // Setup Gamepad
    if (GameInput.isSupported) App.gameInput = new GameInput();

    // Add stats
    var stats = new Stats();
    stage.addChild(stats);

  }

}
