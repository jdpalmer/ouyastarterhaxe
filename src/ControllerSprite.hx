import starling.display.Sprite;
import starling.display.Image;
import starling.display.Button;
import starling.display.Image;
import starling.events.Event;


/**
 * Graphic representation of Ouya Controller
 * @author Mike McMullin
 */
class ControllerSprite extends Sprite {

  var dpads:Array<Image>;
  var joysticks:Array<Image>;
  var buttons:Map<String, Image>;
  
  public function new() {
    super();
    create();
  }
  
  public function create() {
    dpads = [
      createButton("dpad_up.png"),
      createButton("dpad_down.png"),
      createButton("dpad_left.png"),
      createButton("dpad_right.png")
    ];
    
    joysticks = [
      new Image(Assets.getTexture("l_stick.png")),
      new Image(Assets.getTexture("r_stick.png"))
    ];
    
    buttons = [
      Gamepad.BUTTON_ACTION_DOWN => createButton("o.png"),
      Gamepad.BUTTON_ACTION_LEFT => createButton("u.png"),
      Gamepad.BUTTON_ACTION_UP => createButton("y.png"),
      Gamepad.BUTTON_ACTION_RIGHT => createButton("a.png"),
      Gamepad.L1 => createButton("lb.png"),
      Gamepad.R1 => createButton("rb.png"),
      Gamepad.L2 => createButton("lt.png"),
      Gamepad.R2 => createButton("rt.png"),
      Gamepad.L3 => createButton("thumbl.png"),
      Gamepad.R3 => createButton("thumbr.png")
    ];
    
    // add controller gfx
    addChild(new Image(Assets.getTexture("cutter.png")));
    
    // add dpad
    for (dpad in dpads) {
      addChild(dpad);
    }
    
    // add joysticks
    for (joystick in joysticks) {
      addChild(joystick);
    }
    
    // add button gfx
    for (button in buttons) {
      addChild(button);
    }
    
  }
  
  private function createButton(id:String):Image {

    var buttonBmp = new Image(Assets.getTexture(id));
    buttonBmp.visible = false;
    return buttonBmp;

  }
  
  public function updateButton(buttonID:String, visible:Bool) {

    if (!buttons.exists(buttonID)) {
      trace('Uknown button id: $buttonID');
      return;
    }

    buttons[buttonID].visible = visible;

  }
  
  public function updateDpad(x:Float, y:Float) {
    dpads[0].visible = y < 0;
    dpads[1].visible = y > 0;
    dpads[2].visible = x < 0;
    dpads[3].visible = x > 0;
  }
  
  public function updateJoysticks(leftStickX:Float, leftStickY:Float, rightStickX:Float, rightStickY:Float) {

    joysticks[0].x = leftStickX * 10;
    joysticks[0].y = leftStickY * 10;
    joysticks[1].x = rightStickX * 10;
    joysticks[1].y = rightStickY * 10;

  }
  
}
