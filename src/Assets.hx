import starling.textures.Texture;
import flash.display.Bitmap;
import starling.display.Image;
import flash.display.BitmapData;

@:bitmap("assets/a.png")
class BD_a extends flash.display.BitmapData {}

@:bitmap("assets/cutter.png")
class BD_cutter extends flash.display.BitmapData {}

@:bitmap("assets/dpad_down.png")
class BD_dpad_down extends flash.display.BitmapData {}

@:bitmap("assets/dpad_left.png")
class BD_dpad_left extends flash.display.BitmapData {}

@:bitmap("assets/dpad_right.png")
class BD_dpad_right extends flash.display.BitmapData {}

@:bitmap("assets/dpad_up.png")
class BD_dpad_up extends flash.display.BitmapData {}

@:bitmap("assets/l_stick.png")
class BD_l_stick extends flash.display.BitmapData {}

@:bitmap("assets/lb.png")
class BD_lb extends flash.display.BitmapData {}

@:bitmap("assets/lt.png")
class BD_lt extends flash.display.BitmapData {}

@:bitmap("assets/o.png")
class BD_o extends flash.display.BitmapData {}

@:bitmap("assets/r_stick.png")
class BD_r_stick extends flash.display.BitmapData {}

@:bitmap("assets/rb.png")
class BD_rb extends flash.display.BitmapData {}

@:bitmap("assets/rt.png")
class BD_rt extends flash.display.BitmapData {}

@:bitmap("assets/thumbl.png")
class BD_thumbl extends flash.display.BitmapData {}

@:bitmap("assets/thumbr.png")
class BD_thumbr extends flash.display.BitmapData {}

@:bitmap("assets/u.png")
class BD_u extends flash.display.BitmapData {}

@:bitmap("assets/y.png")
class BD_y extends flash.display.BitmapData {}

class Assets {

  public static var WelcomeBackground = WelcomeBackground;

  public static var bitmaps:Map<String,BitmapData> = [
    "a.png" => new BD_a(0, 0),
    "cutter.png" => new BD_cutter(0, 0),
    "dpad_down.png" => new BD_dpad_down(0, 0),
    "dpad_left.png" => new BD_dpad_left(0, 0),
    "dpad_right.png" => new BD_dpad_right(0, 0),
    "dpad_up.png" => new BD_dpad_up(0, 0),
    "l_stick.png" => new BD_l_stick(0, 0),
    "lb.png" => new BD_lb(0, 0),
    "lt.png" => new BD_lt(0, 0),
    "o.png" => new BD_o(0, 0),
    "r_stick.png" => new BD_r_stick(0, 0),
    "rb.png" => new BD_rb(0, 0),
    "rt.png" => new BD_rt(0, 0),
    "thumbl.png" => new BD_thumbl(0, 0),
    "thumbr.png" => new BD_thumbr(0, 0),
    "u.png" => new BD_u(0, 0),
    "y.png" => new BD_y(0, 0)
  ];

  private static var gameTextures = new Map<String, Texture>();
  
  public static function getTexture(name:String):Texture
  {
    if (gameTextures.get(name) == null)
    {      
      gameTextures.set(name, Texture.fromBitmapData(bitmaps.get(name)));
    }
    return gameTextures.get(name);
  }
}
