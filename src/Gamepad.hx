/**
 * @author zeh fernando
 */
class Gamepad {

  // List of typical controls

  /* Directional pad LEFT
   * <p>Style: digital</p>
   * <p>Works in: OUYA Controller (OUYA), Playstation DS3 (OUYA), XBox 360 Controller (OUYA/Android)</p>
   */
  public inline static var DPAD_LEFT = "BUTTON_21";

  /* Directional pad RIGHT
   * <p>Style: digital</p>
   * <p>Works in: OUYA Controller (OUYA), Playstation DS3 (OUYA), XBox 360 Controller (OUYA/Android)</p>
   */
  public inline static var DPAD_RIGHT = "BUTTON_22";

  /* Directional pad UP
   * <p>Style: digital</p>
   * <p>Works in: OUYA Controller (OUYA), Playstation DS3 (OUYA), XBox 360 Controller (OUYA/Android)</p>
   */
  public inline static var DPAD_UP = "BUTTON_19";

  /* Directional pad DOWN
   * <p>Style: digital</p>
   * <p>Works in: OUYA Controller (OUYA), Playstation DS3 (OUYA), XBox 360 Controller (OUYA/Android)</p>
   */
  public inline static var DPAD_DOWN = "BUTTON_20";

  /* Directional pad LEFT
   * <p>Style: pressure-sensitive/analog</p>
   * <p>Works in: Playstation DS3 (OUYA)</p>
   */
  public inline static var DPAD_LEFT_SENSITIVE = "BUTTON_39"; // TODO: this wasn't working. Must test.

  /* Directional pad RIGHT
   * <p>Style: pressure-sensitive/analog</p>
   * <p>Works in: Playstation DS3 (OUYA)</p>
   */
  public inline static var DPAD_RIGHT_SENSITIVE = "BUTTON_37";

  /* Directional pad UP
   * <p>Style: pressure-sensitive/analog</p>
   * <p>Works in: Playstation DS3 (OUYA)</p>
   */
  public inline static var DPAD_UP_SENSITIVE = "BUTTON_36";

  /* Directional pad DOWN
   * <p>Style: pressure-sensitive/analog</p>
   * <p>Works in: Playstation DS3 (OUYA)</p>
   */
  public inline static var DPAD_DOWN_SENSITIVE = "BUTTON_38";

  /* Shoulder button, upper left (LEFT 1/LEFT BUTTON)
   * <p>Style: digital</p>
   * <p>Works in: OUYA Controller (OUYA), Playstation DS3 (OUYA), XBox 360 Controller (OUYA/Android)</p>
   */
  public inline static var L1 = "BUTTON_102";

  /* Shoulder button, upper right (RIGHT 1/RIGHT BUTTON)
   * <p>Style: digital</p>
   * <p>Works in: OUYA Controller (OUYA), Playstation DS3 (OUYA), XBox 360 Controller (OUYA/Android)</p>
   */
  public inline static var R1 = "BUTTON_103";

  /* Shoulder button, lower left (LEFT 2/LEFT TRIGGER)
   * <p>Style: digital</p>
   * <p>Works in: OUYA Controller (OUYA), Playstation DS3 (OUYA)</p>
   * <p>Note: this doesn't work on the XBox 360 Controller (OUYA). Use L2_SENSITIVE instead.</p>
   */
  public inline static var L2 = "BUTTON_104";

  /* Should button, lower right (RIGHT 2/RIGHT TRIGGER)
   * <p>Style: digital</p>
   * <p>Works in: OUYA Controller (OUYA), Playstation DS3 (OUYA)</p>
   * <p>Note: this doesn't work on the XBox 360 Controller (OUYA). Use R2_SENSITIVE instead.</p>
   */
  public inline static var R2 = "BUTTON_105";

  /* Left stick press (LEFT 3)
   * <p>Style: digital</p>
   * <p>Works in: OUYA Controller (OUYA), Playstation DS3 (OUYA), XBox 360 Controller (OUYA/Android)</p>
   */
  public inline static var L3 = "BUTTON_106";

  /* Right stick press (RIGHT 3)
   * <p>Style: digital</p>
   * <p>Works in: OUYA Controller (OUYA), Playstation DS3 (OUYA), XBox 360 Controller (OUYA/Android)</p>
   */
  public inline static var R3 = "BUTTON_107";

  /* Shoulder button, lower left (LEFT 2/LEFT TRIGGER)
   * <p>Style: pressure-sensitive/analog</p>
   * <p>Works in: OUYA Controller (OUYA), Playstation DS3 (OUYA), XBox 360 Controller (OUYA/Android)</p>
   */
  public inline static var L2_SENSITIVE = "AXIS_17";

  /* Shoulder button, lower right (RIGHT 2/RIGHT TRIGGER)
   * <p>Style: pressure-sensitive/analog</p>
   * <p>Works in: OUYA Controller (OUYA), Playstation DS3 (OUYA), XBox 360 Controller (OUYA/Android)</p>
   */
  public inline static var R2_SENSITIVE = "AXIS_18";

  /* Left analog stick, horizontal (X) axis. The value received is for a left-to-right position.
   * <p>Style: pressure-sensitive/analog</p>
   * <p>Works in: OUYA Controller (OUYA), Playstation DS3 (OUYA), XBox 360 Controller (OUYA/Android), XBox 360 Controller (Windows)</p>
   */
  public inline static var STICK_LEFT_X = "AXIS_0";

  /* Left analog stick, vertical (Y) axis. The value received is for a up-to-down position.
   * <p>Style: pressure-sensitive/analog</p>
   * <p>Works in: OUYA Controller (OUYA), Playstation DS3 (OUYA), XBox 360 Controller (OUYA/Android), XBox 360 Controller (Windows)</p>
   * <p>Note: on Windows, when using the XBox 360 controller, the orientation is reversed: the value position is down-to-up.</p>
   */
  public inline static var STICK_LEFT_Y = "AXIS_1";

  /* Right analog stick, horizontal (X) axis. The value received is for a left-to-right position.
   * <p>Style: pressure-sensitive/analog</p>
   * <p>Works in: OUYA Controller (OUYA), Playstation DS3 (OUYA), XBox 360 Controller (OUYA/Android)</p>
   */
  public inline static var STICK_RIGHT_X = "AXIS_11";

  /* Right analog stick, vertical (Y) axis. The value received is for a up-to-down position.
   * <p>Style: pressure-sensitive/analog</p>
   * <p>Works in: OUYA Controller (OUYA), Playstation DS3 (OUYA), XBox 360 Controller (OUYA/Android)</p>
   */
  public inline static var STICK_RIGHT_Y = "AXIS_14";

  /* Action button, down (O/green in the OUYA, Cross/blue in the Playstation, A/green in the XBox 360)
   * <p>Style: digital</p>
   * <p>Works in: OUYA Controller (OUYA), Playstation DS3 (OUYA), XBox 360 Controller (OUYA/Android)</p>
   */
  public inline static var BUTTON_ACTION_DOWN = "BUTTON_96";

  /* Action button, right (A/red in the OUYA, Circle/red in the Playstation, B/red in the XBox 360)
   * <p>Style: digital</p>
   * <p>Works in: OUYA Controller (OUYA), Playstation DS3 (OUYA), XBox 360 Controller (OUYA/Android)</p>
   */
  public inline static var BUTTON_ACTION_RIGHT = "BUTTON_97";

  /* Action button, up (Y/yellow in the OUYA, Triangle/green in the Playstation, Y/yellow in the XBox 360)
   * <p>Style: digital</p>
   * <p>Works in: OUYA Controller (OUYA), Playstation DS3 (OUYA), XBox 360 Controller (OUYA/Android)</p>
   */
  public inline static var BUTTON_ACTION_UP = "BUTTON_100";

  /* Action button, left (U/blue in the OUYA, Square/purple in the Playstation, X/blue in the XBox 360)
   * <p>Style: digital</p>
   * <p>Works in: OUYA Controller (OUYA), Playstation DS3 (OUYA), XBox 360 Controller (OUYA/Android)</p>
   */
  public inline static var BUTTON_ACTION_LEFT = "BUTTON_99";

  /* Start button
   * <p>Style: digital</p>
   * <p>Works in: Playstation DS3 (OUYA)</p>
   * <p>Note: this is also present on the XBox 360 controller (OUYA), but never used.</p>
   */
  public inline static var START = "BUTTON_108";


  // Unknowns: listed in some device but never used

  /* Unknown control; declared by the GameInput API, but never used
   * <p>Style: ?</p>
   * <p>Works in: OUYA Controller, Playstation DS3</p>
   */
  public inline static var BUTTON_UNKNOWN_BUTTON_32 = "BUTTON_32";

  /* Unknown control; declared by the GameInput API, but never used
   * <p>Style: ?</p>
   * <p>Works in: OUYA Controller, Playstation DS3</p>
   */
  public inline static var BUTTON_UNKNOWN_BUTTON_33 = "BUTTON_33";

  /* Unknown control; declared by the GameInput API, but never used
   * <p>Style: ?</p>
   * <p>Works in: OUYA Controller, Playstation DS3</p>
   */
  public inline static var BUTTON_UNKNOWN_BUTTON_34 = "BUTTON_34";

  /* Unknown control; declared by the GameInput API, but never used
   * <p>Style: ?</p>
   * <p>Works in: OUYA Controller, Playstation DS3</p>
   */
  public inline static var BUTTON_UNKNOWN_BUTTON_35 = "BUTTON_35";


  // PC-only. Move to a separate class?

  /* Action button, down (A/green)
   * <p>Style: digital</p>
   * <p>Works in: XBox 360 Controller (Windows)</p>
   */
  public inline static var XBOX_BUTTON_ACTION_A = "BUTTON_4";

  /* Action button, right (B/red)
   * <p>Style: digital</p>
   * <p>Works in: XBox 360 Controller (Windows)</p>
   */
  public inline static var XBOX_BUTTON_ACTION_B = "BUTTON_5";

  /* Action button, up (Y/yellow)
   * <p>Style: digital</p>
   * <p>Works in: XBox 360 Controller (Windows)</p>
   */
  public inline static var XBOX_BUTTON_ACTION_Y = "BUTTON_7";

  /* Action button, left (X/blue)
   * <p>Style: digital</p>
   * <p>Works in: XBox 360 Controller (Windows)</p>
   */
  public inline static var XBOX_BUTTON_ACTION_X = "BUTTON_6";

  /* Shoulder button, upper left (LEFT 1)
   * <p>Style: digital</p>
   * <p>Works in: XBox 360 Controller (Windows)</p>
   */
  public inline static var XBOX_L1 = "BUTTON_8";

  /* Shoulder button, upper right (RIGHT 1)
   * <p>Style: digital</p>
   * <p>Works in: XBox 360 Controller (Windows)</p>
   */
  public inline static var XBOX_R1 = "BUTTON_9";

  /* Shoulder button, lower left (LEFT 2)
   * <p>Style: pressure-sensitive/analog</p>
   * <p>Works in: XBox 360 Controller (Windows)</p>
   */
  public inline static var XBOX_L2_SENSITIVE = "BUTTON_10";

  /* Shoulder button, lower right (RIGHT 2)
   * <p>Style: pressure-sensitive/analog</p>
   * <p>Works in: XBox 360 Controller (Windows)</p>
   */
  public inline static var XBOX_R2_SENSITIVE = "BUTTON_11";

  /* Left stick press (LEFT 3)
   * <p>Style: digital</p>
   * <p>Works in: XBox 360 Controller (Windows)</p>
   */
  public inline static var XBOX_L3 = "BUTTON_14";

  /* Right stick press (RIGHT 3)
   * <p>Style: digital</p>
   * <p>Works in: XBox 360 Controller (Windows)</p>
   */
  public inline static var XBOX_R3 = "BUTTON_15";

  /* Directional pad LEFT
   * <p>Style: digital</p>
   * <p>Works in: XBox 360 Controller (Windows)</p>
   */
  public inline static var XBOX_DPAD_LEFT = "BUTTON_18";

  /* Directional pad RIGHT
   * <p>Style: digital</p>
   * <p>Works in: XBox 360 Controller (Windows)</p>
   */
  public inline static var XBOX_DPAD_RIGHT = "BUTTON_19";

  /* Directional pad UP
   * <p>Style: digital</p>
   * <p>Works in: XBox 360 Controller (Windows)</p>
   */
  public inline static var XBOX_DPAD_UP = "BUTTON_16";

  /* Directional pad DOWN
   * <p>Style: digital</p>
   * <p>Works in: XBox 360 Controller (Windows)</p>
   */
  public inline static var XBOX_DPAD_DOWN = "BUTTON_17";

  /* BACK middle game controller button
   * <p>Style: digital</p>
   * <p>Works in: XBox 360 Controller (Windows)</p>
   */
  public inline static var XBOX_BACK = "BUTTON_12";

  /* START middle game controller button
   * <p>Style: digital</p>
   * <p>Works in: XBox 360 Controller (Windows)</p>
   */
  public inline static var XBOX_START = "BUTTON_13";

}
