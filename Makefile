##
## This section has common project settings you may need to change.
##

FLEX_SDK=/opt/flex-sdk

ANDROID_SDK=/opt/android-sdk

APP=OuyaControllerTest

APP_XML=$(APP).xml

SOURCES=src/App.hx src/ControllerSprite.hx src/Assets.hx src/Gamepad.hx

##
## It's less common that you would need to change anything after this line.
##

SIGN_CERT=sign.pfx

SIGN_PWD=abc123

SWF_VERSION=11.8

ADL=$(FLEX_SDK)/bin/adl

ADT=$(FLEX_SDK)/bin/adt

##
## Build rules
##

all: $(APP).swf

apk: $(APP).apk

clean:
	rm -rf $(APP).swf $(APP).apk

test: $(APP).swf
	$(ADL) -profile tv -screensize 1920x1080:1920x1080 $(APP_XML)

sign.pfx:
	$(ADT) -certificate -validityPeriod 25 -cn SelfSigned 1024-RSA $(SIGN_CERT) $(SIGN_PWD)


install: $(APP).apk
	$(ADT) -installApp -platform android -platformsdk $(ANDROID_SDK) -package $(APP).apk

$(APP).swf: $(SOURCES)
	haxe -v -cp src -cp vendor -swf-version $(SWF_VERSION) -swf-header 1920:1080:60:000000 -main App -swf $(APP).swf -swf-lib vendor/starling.swc --macro "patchTypes('vendor/starling.patch')"

$(APP).apk: $(APP).swf sign.pfx
	$(ADT) -package -target apk-captive-runtime -storetype pkcs12 -keystore $(SIGN_CERT) $(APP).apk $(APP_XML) $(APP).swf
