This project is a sort of "Hello World" for Ouya development.

1. Install the Flex SDK.  The Makefile defaults to /opt/flex-sdk.  If
you install it somewhere else, just update the FLEX_SDK variable in
the Makefile.

    http://flex.apache.org/

2. Install Haxe 3.

    http://haxe.org/download

3. (OPTIONAL) Install the Android SDK. You only need this if you want
to install your application via USB for quick debugging.  The Makefile
defaults to /opt/android-sdk.  You can change this with the
ANDROID_SDK variable in the Makefile.

4. To build the SWF type:

    make

5. To test the SWF locally type:

    make test

6. To build an APK for deployment type:

    make apk

AUTHORS

* GamePad code by Zeh Fernando
* ControllerSprite by Mike McMullin
* Performance Monitor by Mr. Doob
* Other code by James Palmer

REFERENCES

* http://www.adobe.com/devnet/air/articles/game-controllers-on-air.html
